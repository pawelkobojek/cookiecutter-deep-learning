#+TITLE: Deep Learning experimentation cookiecutter
#+AUTHOR: Paweł Kobojek
* What is this?
A skeleton for Deep Learning experimentations. This is not viable for large-scale deep learning experiments but rather for validation of ideas.
* Introduction
As with pretty much every other technology, deep learning experimentation project tend to involve fair amount of boilerplate, not always mitigated by the frameworks. I've established a skeleton of a project which I use for ideas validation and found myself rewritting/copying over files to new projects. Since the idea validation phase of machine learning development should be as quick as possible, I prepared this skeleton.
* Quickstart
1. Create a new project by running ~cookiecutter gl:pawelkobojek/cookiecutter-deep-learning~
2. Copy ~docs/sample.env~ to the main directory (the one in which ~Makefile~ resides) and name it ~.env~.
3. Change the contents of the ~.env~ file according to your needs.
4. Create a file named ~$DATA_PATH/configs/config.toml~ (default: ~$PWD/volumes/data/config.toml~) and configure the experiments according to your needs (see: ~docs/sample_config.toml~). ~DATA_PATH~ is taken from ~.env~ file (default is ~$PWD/volumes/data~).
5. Run ~make build~ (~make build-cpu~ for CPU only version) in order to build Docker images.
6. Run ~make train~ (~make train-cpu~ for CPU only version) to run training.
7. Run ~make visualize~ to run visualization. Tensorboard will be running at port 6006.
8. To stop everything run ~make stop-all~
* Alternatives
- [[https://drivendata.github.io/cookiecutter-data-science/][cookiecutter-data-science]]
