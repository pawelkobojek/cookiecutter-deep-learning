import toml


# TODO - move to separate module; use actual object instead of dict
def read_config(config_path):
    """Builds a Config object from toml configuration file

    Args:
      config_path: path to the toml configuration file

    Returns:
      Config object
    """
    with open(config_path, 'r') as f:
        conf_dict = toml.loads(f.read())
    return conf_dict
