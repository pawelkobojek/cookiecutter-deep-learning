import toml
import json
import re
import random
import os

import numpy as np
import tensorflow as tf

from datetime import datetime

from keras.callbacks import TensorBoard, TerminateOnNaN

from {{cookiecutter.package_name}} import data, model_builder


def prepare_experiment(config, experiments_dir_path='/experiments'):
    """Prepares directory for experiment and copies over the config.
    If experiment_name in config is not specified it defaults
    to current datetime in isoformat.

    Returns:
      Path to the experiments directory
    """
    if 'random_seed' not in config:
        seed = random.randint(0, 999999999)
        config['random_seed'] = seed

    np.random.seed(config['random_seed'])
    tf.set_random_seed(config['random_seed'])
    experiment_name = config.get('experiment_name')
    if not experiment_name:
        experiment_name = datetime.now().isoformat()

    placeholders = re.findall(r'(\$\(([A-z0-9_]+)\))', experiment_name)
    for placeholder, key in placeholders:
        if key in config:
            experiment_name = experiment_name.replace(placeholder,
                                                      str(config[key]))

    config['experiment_name'] = experiment_name

    experiment_path = os.path.join(experiments_dir_path, experiment_name)
    if os.path.exists(experiment_path):
        return experiment_path

    os.makedirs(experiment_path)

    with open(os.path.join(experiment_path, 'config.toml'),
              'w') as config_file:
        config_file.write(toml.dumps(config))

    return experiment_path


def execute(config):
    """Executes the experiment represented by given config.

    Args:
       config: configuration dictionary
    """
    experiment_path = prepare_experiment(config)
    print('Using config:', config)

    X_train, y_train, X_dev, y_dev, X_test, y_test = data.read_data(
        config['dataset'], config['num_classes'])

    model = model_builder.build_model(config)

    callbacks = [TensorBoard(log_dir=experiment_path), TerminateOnNaN()]

    history = model.fit(
        X_train,
        y_train,
        batch_size=config['batch_size'],
        epochs=config['epochs'],
        validation_data=(X_dev, y_dev),
        callbacks=callbacks)

    with open(os.path.join(experiment_path, 'history.json'), 'w') as f:
        f.write(json.dumps(history.history))

    model.save(os.path.join(experiment_path, 'model.h5'))
    with open(os.path.join(experiment_path, 'architecture.yaml'), 'w') as f:
        f.write(model.to_yaml())
