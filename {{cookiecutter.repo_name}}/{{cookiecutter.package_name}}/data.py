import os
import pandas as pd
import numpy as np

from sklearn import preprocessing
from keras.utils import to_categorical

dataset_map = {
    'default': {
        'path': '/data',
    },
    'random': {
        'shape': (10, 2),
    },
}


def _read_dataset_part(data_path, name):
    """Reads dataset part (train, dev or test)

    Args:
      name: name of the dataset part. Either "train", "dev" or "test"

    Returns:
      Pandas DataFrame representing desired dataset part.
    """
    set_name_template = 'cv-valid-{0}-ready.csv'
    return pd.read_csv(os.path.join(data_path, set_name_template.format(name)))


def read_data(dataset_name, num_classes):
    """Reads data from CSV files.

    Args:
      dataset_name: dataset name (see dataset_map in this file)

    Returns:
      X_train: training dataset
      y_train: training labels
      X_dev:   dev dataset
      y_dev:   dev labels
      X_test:  test dataset
      y_test:  test labels
    """
    dataset = dataset_map[dataset_name]
    if dataset_name == 'random':
        classes = np.random.random_integers(0, num_classes - 1,
                                            dataset['shape'][0])
        X = np.random.normal(size=dataset['shape'])
        y = to_categorical(classes, num_classes=num_classes)
        return X, y, X, y, X, y

    data_path = dataset['path']
    features_count = dataset['features_count']
    returns = []
    for name in ['train', 'dev', 'test']:
        dataset = _read_dataset_part(data_path, name)
        dataset = dataset.dropna(how='any')
        dataset = dataset[dataset['id'] < num_classes]
        y_dataset = dataset['id'].values
        y_dataset = to_categorical(y_dataset, num_classes=num_classes)
        X_dataset = dataset.drop('id', axis=1).values
        X_dataset = preprocessing.scale(X_dataset)
        X_dataset = X_dataset.reshape(-1, features_count, 1)
        returns.append(X_dataset)
        returns.append(y_dataset)

    return tuple(returns)
