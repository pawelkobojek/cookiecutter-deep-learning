#!/usr/bin/env python
import argparse
import os

from {{cookiecutter.package_name}} import configuration, experiment


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument(
        '--config-path',
        help='Path to the configuration file',
        default='/data/configs/config.toml')
    parser.add_argument('--gpu', help='ID of gpu to use', type=int, default=0)
    return parser.parse_args()


def iterate_config(config):
    executed = False
    for key, value in config.items():
        if isinstance(value, list):
            executed = True
            for current_setting in value:
                current_config = config.copy()
                current_config[key] = current_setting
                iterate_config(current_config)
            break

    if not executed:
        experiment.execute(config)


def main(args):
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)

    config = configuration.read_config(args.config_path)

    iterate_config(config)


if __name__ == '__main__':
    main(parse_arguments())
