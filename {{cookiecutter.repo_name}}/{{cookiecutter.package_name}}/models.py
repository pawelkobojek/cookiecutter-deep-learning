from keras.models import Sequential
from keras.layers import LSTM, Dense, Activation, Dropout


def small_model(config):
    model = Sequential()
    model.add(Dense(config['num_classes']))
    model.add(Activation('softmax'))
    return model


def big_model(config):
    model = Sequential()
    model.add(LSTM(4096))
    if 'dropout' in config:
        model.add(Dropout(config['dropout_rate']))
    model.add(Dense(8192))
    if 'dropout' in config:
        model.add(Dropout(config['dropout_rate']))
    model.add(Dense(config['num_classes']))
    model.add(Activation('softmax'))
    return model


model_map = {
    'small': small_model,
    'big': big_model,
}
