from {{cookiecutter.package_name}} import models
from keras import optimizers


def build_model(config):
    """Builds and compiles Keras model

    Args:
      config: Config object. Defines model's hyperparams.

    Returns:
      Compiled Keras model
    """

    model = models.model_map[config['model']](config)
    model.compile(
        optimizer=optimizers.Adam(lr=config['learning_rate']),
        loss='categorical_crossentropy',
        metrics=['accuracy'])

    return model
