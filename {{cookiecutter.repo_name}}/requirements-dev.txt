toml
keras
pandas
sklearn
tensorflow=={{cookiecutter.tensorflow_version}}
jupyter
seaborn

ipython
jedi
rope
autopep8
flake8
importmagic
yapf
