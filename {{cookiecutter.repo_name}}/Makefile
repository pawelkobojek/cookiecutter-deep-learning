ENV_FILE=.env
IMAGE={{cookiecutter.docker_image_name}}
CONTAINER_NAME={{cookiecutter.repo_name}}
PORT=6006
GPU=0
CONFIG_PATH=/data/configs/config.toml

include $(ENV_FILE)

build: .env
	docker build -f Dockerfile.gpu -t $(IMAGE) .

build-cpu: .env
	docker build -f Dockerfile.cpu -t $(IMAGE) .

train:
	docker run \
	--runtime=nvidia \
	--name=$(CONTAINER_NAME)-train \
	-it \
	-v $(EXPERIMENTS_PATH):/experiments \
	-v $(DATA_PATH):/data \
	$(IMAGE) \
	python {{cookiecutter.package_name}}/train.py --gpu=$(GPU) --config-path=$(CONFIG_PATH)

train-cpu:
	docker run \
	--name=$(CONTAINER_NAME)-train \
	--rm \
	-it \
	-v $(EXPERIMENTS_PATH):/experiments \
	-v $(DATA_PATH):/data \
	$(IMAGE) \
	python {{cookiecutter.package_name}}/train.py

eval:
	docker run \
	--runtime=nvidia \
	--name=$(CONTAINER_NAME)-eval \
	-it \
	-v $(EXPERIMENTS_PATH):/experiments \
	-v $(DATA_PATH):/data \
	$(IMAGE) \
	python {{cookiecutter.package_name}}/eval.py

eval-cpu:
	docker run \
	--name=$(CONTAINER_NAME)-eval \
	-it \
	-v $(EXPERIMENTS_PATH):/experiments \
	-v $(DATA_PATH):/data \
	$(IMAGE) \
	python {{cookiecutter.package_name}}/eval.py

visualize:
	docker run \
	--name=$(CONTAINER_NAME)-visualize \
	--rm \
	-p $(PORT):6006 \
	-idt -v $(EXPERIMENTS_PATH):/experiments \
	tensorflow/tensorflow:{{cookiecutter.tensorflow_version}}-py3 \
	tensorboard --logdir=/experiments

bash:
	docker run \
	--entrypoint=/bin/bash \
	--name=$(CONTAINER_NAME)-bash \
	--rm \
	-it \
	-v $(DATA_PATH):/data \
	-v $(EXPERIMENTS_PATH):/experiments \
	$(IMAGE)

test:
	docker run \
	--entrypoint=python \
	--rm \
	-it \
	-v $(DATA_PATH):/data \
	$(IMAGE) \
	-m unittest $(TEST)

stop-all:
	docker stop $(CONTAINER_NAME)-train ; \
	docker rm $(CONTAINER_NAME)-train ; \
	docker stop $(CONTAINER_NAME)-eval ; \
	docker rm $(CONTAINER_NAME)-eval ; \
	docker stop $(CONTAINER_NAME)-visualize ; \
	docker rm $(CONTAINER_NAME)-visualize || true

notebooks:
	docker run \
	--name=$(CONTAINER_NAME)-notebooks
	--entrypoint=jupyter \
	--rm \
	-it \
	-p $(PORT):8888 \
	-v $(DATA_PATH):/data \
	-v $(EXPERIMENTS_PATH):/experiments \
	-v $(NOTEBOOKS_PATH):/notebooks \
	$(IMAGE) \
	notebook --allow-root --notebook-dir=/notebooks

all: build run

.PHONY: build build-cpu train train-cpu eval eval-cpu bash stop-all test notebooks
